// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyD6Hb_KbQ9hX30NxXEE20kR3zEU_qmH0zU",
    authDomain: "plawer-a2bb5.firebaseapp.com",
    databaseURL: "https://plawer-a2bb5.firebaseio.com",
    projectId: "plawer-a2bb5",
    storageBucket: "plawer-a2bb5.appspot.com",
    messagingSenderId: "965804703896",
    appId: "1:965804703896:web:427ac12b93faf65e"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
