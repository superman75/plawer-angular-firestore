import { Component, OnInit } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { ActivatedRoute, Router } from '@angular/router';
import { ProjectService } from '../shared/project.service';
import { Project } from '../shared/project.model';
import { AngularFirestore } from '@angular/fire/firestore';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {

  allProjects: Project[] = [];
  active_id = "";
  constructor(private _router: Router, private route: ActivatedRoute, private service: ProjectService, private firestore: AngularFirestore) { 

  }

  ngOnInit() {
    this.service.getProjects().subscribe(actionArray => {
      this.allProjects = actionArray.map(item=>{
        return {
          id: item.payload.doc.id,
          ...item.payload.doc.data()
        } as Project
      });
      this.allProjects.sort(function(a, b){ return a.index-b.index});
      // when there is no active one.
      //console.log("active id : " + this.active_id);
      if(!this.active_id) {
        for(let project of this.allProjects){
          if(!project.is_sect){
            this.active_id = project.id;
            this._router.navigateByUrl(`${this.active_id}`)
            return;
          }
        }
      }
    })
  }

  openDashboard(projectid : string) {
    this.active_id = projectid;
  }
  drop(event: CdkDragDrop<any[]>) {  
    moveItemInArray(this.allProjects, event.previousIndex, event.currentIndex);  
    let index = 0;
    for(let project of this.allProjects){
      project.index = index++;
      this.firestore.doc('projects/' + project.id).update(project);
    }
  }  
}
