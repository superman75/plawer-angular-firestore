import { Component } from '@angular/core';
import * as $ from 'jquery';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Plawer';
  ngOnInit() {
    var height = window.innerHeight;
    document.getElementsByClassName('container')[0].setAttribute("style","min-height:" + height + 'px');

    function KeyPress(e) {
      var evtobj = window.event? event : e
      //if (evtobj.keyCode == 78 && evtobj.ctrlKey) {
      //  if (evtobj.keyCode == 78) {
        if (evtobj.keyCode == 78 && evtobj.shiftKey) {
        //alert("Ctrl+z");
        let taskInput = document.getElementById("txtAddNewTask");
        if (taskInput !== document.activeElement){
          taskInput.focus();
        }
      }

  }
  
    document.onkeyup = KeyPress;

  }

}
