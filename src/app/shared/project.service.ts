import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
@Injectable({
  providedIn: 'root'
})
export class ProjectService {

  constructor(private firestore: AngularFirestore) { }

  getProjects() {
    return this.firestore.collection('projects').snapshotChanges();
  }

  getProject(projectId: string){
    return this.firestore.collection('projects', ref=> ref.where("id","==", projectId)).snapshotChanges();
  }

  getTasks(projectId: string){
    return this.firestore.collection('tasks', ref=> ref.where("project_id","==", projectId).where("is_archived", "==", false)).snapshotChanges();
  }
}
