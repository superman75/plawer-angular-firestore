export class Project {
    id: string;
    image_url: string;
    is_sect: boolean;
    project_name: string;
    index: number;
    description: string;
}

export class Task {
    id: string;
    is_sect: string;
    task_name: string;
    index: number;
    status: string;
    disabled: boolean;
    interested: boolean;
    project_id: string;
    is_archived: boolean;
}
