import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CdkDragDrop, moveItemInArray  } from '@angular/cdk/drag-drop';
import { FormControl } from "@angular/forms";
import { ProjectService } from '../shared/project.service';
import { Task } from '../shared/project.model';
import { AngularFirestore } from '@angular/fire/firestore';
import { MatMenuTrigger } from '@angular/material';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  
  @ViewChild(MatMenuTrigger)
  contextMenu: MatMenuTrigger;
  contextMenuPosition = { x: '0px', y: '0px' };
  
  private sub: any;
  private projectId : string;
  private projectName : string;
  private projectDescription : string;
  allTasks: Task[] = [];
  newTask : string ;
  constructor(private route: ActivatedRoute, private service: ProjectService, private firestore: AngularFirestore) { }

  ngOnInit() {

    var height = window.innerHeight;
    document.getElementById('dashboard').style.minHeight = height + 'px';

    this.sub = this.route.params.subscribe(params=>{
      this.projectId = params['projectid'];      
      
      
      this.service.getProject(this.projectId).subscribe(actionArray => {
        actionArray.map(item=>{
          console.log(item.payload.doc.data().project_name);        
          //this.projectName = item.payload.doc.data().project_name;
          this.projectName = item.payload.doc.data().project_name;
          this.projectDescription = item.payload.doc.data().description;
        });        
      });
      

      this.service.getTasks(this.projectId).subscribe(actionArray => {
        this.allTasks = actionArray.map(item=>{
          return {
            id: item.payload.doc.id,
            ...item.payload.doc.data()
          } as Task
        })
        this.allTasks.sort(function(a, b){ return a.index-b.index});
      });

    });
  }

  updateInterested(task: any){
    task.interested = !task.interested;
    this.firestore.doc("tasks/" + task.id).update(task);
  }

  updateStatus(task:any) {
    if(task.status=="progress"){
      task.status="completed"
    } else {
      task.status="progress"
    }
    this.firestore.doc("tasks/" + task.id).update(task);    
  }
  onContextMenu(event: MouseEvent, task: Task) {
    event.preventDefault();
    this.contextMenuPosition.x = event.clientX + 'px';
    this.contextMenuPosition.y = event.clientY + 'px';
    this.contextMenu.menuData = { 'item': task };
    this.contextMenu.openMenu();
  }

  onContextMenuAction_DeleteTask(task: Task) {
    this.firestore.doc("tasks/" + task.id).delete();
  }

  onContextMenuAction_ArchiveTask(task: Task) {
    task.status = "completed";
    task.is_archived = true;
    this.firestore.doc("tasks/" + task.id).update(task);  
  }

  onContextMenuAction2(task: Task) {
    task.disabled = !task.disabled;
    this.firestore.doc("tasks/" + task.id).update(task);     
  }
  drop(event: CdkDragDrop<any[]>) {  
    moveItemInArray(this.allTasks, event.previousIndex, event.currentIndex); 
    let index = 0;
    for(let task of this.allTasks){
      task.index = index++;
      this.firestore.doc('tasks/' + task.id).update(task);
    } 
  }  

  eventHandler($event) {
    if($event.keyCode == 13) {

      if(!this.newTask){
        return;
      }
      let issect = false;
      if(this.newTask.charAt(this.newTask.length-1)==':'){
        this.newTask = this.newTask.slice(0, -1);
        issect = true;
      } else if(this.newTask.charAt(this.newTask.length-1)==' ' && this.newTask.charAt(this.newTask.length-2)==':'){
        this.newTask = this.newTask.slice(0, -2);
        issect = true;
      }
      
      for(let task of this.allTasks){
        task.index = task.index+1;
        this.firestore.doc('tasks/'+task.id).update(task);
      }

      this.firestore.collection("tasks").add(
        {
          task_name: this.newTask,
          is_sect: issect,
          status: 'progress',
          interested: false,
          disabled: false,
          index: 0,
          project_id: this.projectId,
          is_archived: false
        }
      )
      this.newTask = '';
    }
  }

}
